#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
__author__ = 'Ma.YL'
class Dvision:

    def __init__(self, code='',dname = '',pcode=''):
        self.__code = code
        self.__name = dname
        self.__pcode = pcode
        self.__level = self.__GetDvision()
        self.ocode = code
    def obj2dict(self,full=False):
        if full:
            return {
                'code':self.__code,
                'name':self.__name,
                #'level':self.__level,
                'pcode':self.__pcode
            }
        else:
            return {
                'name':self.__name
            }
    def __str__(self):
        reload(sys)
        sys.setdefaultencoding('utf-8')
        return u"code:{0},name:{1},pcode:{2}".format(self.__code,self.__name,self.__pcode)
    def __GetDvision(self):
        try:
            if self.__code != '' and len(self.__code) == 6:
                return "p" if self.__code[2:] == "0000" else "c" if self.__code[4:] == "00" else "d"
            else:
                return ""
        except:
            return ""
    def GetSubDvision(self,dictDvision):
        __cityFilter = (u'自治区直辖县级行政区划', u'县', u'省直辖县级行政区划', u'市辖区')
        list = []
        subDvision = {}
        levelInt = 2 if self.__level == "p" else 4 if self.__level == "c" else 0
        if(self.__name in (u'北京市',u'重庆市',u'天津市',u'上海市')):
            subDvision = dict((k, v) for (k, v) in dictDvision.items() if (
            self.__code[0:levelInt] == k[0:levelInt] and k[levelInt:] != "0" * (
            6 - levelInt) and v not in __cityFilter))
        elif levelInt == 2:
            subDvision = dict((k, v) for (k, v) in dictDvision.items() if (
                self.__code[0:2] == k[0:2] and (k[2:4] != "0" * 2 and k[4:] == "00" or k[2:4]=='90' and k[4:]!='00') and v not in __cityFilter))
        elif levelInt == 4:
            subDvision = dict((k, v) for (k, v) in dictDvision.items() if (
                self.__code[0:4] == k[0:4] and k[4:] != "0" * 2 and v not in __cityFilter))
        for key in sorted(subDvision):
            dvi = Dvision(key,subDvision.get(key),self.__code)
            list.append(dvi)
        return list
